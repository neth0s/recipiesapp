package com.example.myapplication;

import android.os.Bundle;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.myapplication.databinding.ActivityRecipiesBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class RecipiesActivity extends AppCompatActivity {

    protected ArrayList<Ingredient> ingredients; //List of user ingredients

    protected int number; //Number of recipies to request
    protected boolean ignorePantry; //Wether to ignore pantry ingredients

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ingredients = new ArrayList<>();
        initListViewData();
        setNumber(3);
        setIgnorePantry(true);

        com.example.myapplication.databinding.ActivityRecipiesBinding binding =
                ActivityRecipiesBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_settings)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_recipies);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);
    }

    public ArrayList<Ingredient> getIngredients(){
        return ingredients;
    }

    public int getNumber() {
        return number;
    }

    public boolean getPantry(){
        return ignorePantry;
    }

    public void addIngredient(Ingredient i){
        ingredients.add(i);
    }

    public void setNumber(int number){
        this.number = number;
    }

    public void setIgnorePantry(boolean ignore){
        this.ignorePantry = ignore;
    }

    private void initListViewData()  {
        addIngredient(new Ingredient(9003, "apple", false));
        addIngredient(new Ingredient(9037, "avocado", false));
        addIngredient(new Ingredient(10123, "bacon", false));
        addIngredient(new Ingredient(11252, "lettuce", false));
        addIngredient(new Ingredient(11529, "tomatoes", false));
    }
}