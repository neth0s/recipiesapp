package com.example.myapplication.ui.home;

import static androidx.constraintlayout.motion.utils.Oscillator.TAG;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.myapplication.Ingredient;
import com.example.myapplication.RecipiesActivity;
import com.example.myapplication.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;
    private ListView listView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //HomeViewModel homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        listView = binding.listView;
        this.listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        this.listView.setOnItemClickListener((parent, view, position, id) -> {
            Log.i(TAG, "onItemClick: " +position);
            CheckedTextView v = (CheckedTextView) view;
            boolean currentCheck = v.isChecked();
            Ingredient ingredient = (Ingredient) listView.getItemAtPosition(position);
            ingredient.setActive(!currentCheck);
        });

        RecipiesActivity act = (RecipiesActivity) getActivity();
        assert act != null;

        ArrayAdapter<Ingredient> arrayAdapter =
                new ArrayAdapter<>(this.getContext(), android.R.layout.simple_list_item_checked, act.getIngredients());
        listView.setAdapter(arrayAdapter);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}