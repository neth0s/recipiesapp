package com.example.myapplication.ui.dashboard;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.myapplication.RecipiesActivity;
import com.example.myapplication.databinding.FragmentDashboardBinding;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DashboardFragment extends Fragment {

    private FragmentDashboardBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        DashboardViewModel dashboardViewModel = new ViewModelProvider(this).get(DashboardViewModel.class);

        binding = FragmentDashboardBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        Button button = binding.button;
        button.setOnClickListener(v -> {
            Thread t = new Thread(() -> {
                URL url;
                try {
                    String urlAdress = "https://api.spoonacular.com/recipes/findByIngredients";
                    urlAdress += getRequestParameters();

                    url = new URL(urlAdress);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    try {
                        InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                        String s = DashboardFragment.this.readStream(in);
                        Log.i("URL Request", s);
                        Toast.makeText(getActivity(),
                                "Sent Request to Spoonacular API", Toast.LENGTH_LONG).show();
                    } finally {
                        urlConnection.disconnect();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            t.start();
        });

        final TextView textView = binding.textDashboard;
        dashboardViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        return root;
    }

    private String getRequestParameters(){
        RecipiesActivity act = (RecipiesActivity) getActivity();
        assert act != null;

        return "?apiKey=" + "384748509b4e41d48c0dd55647fe91d3" +
                "&ingredients=" + act.getIngredients() +
                "&ignorePantry=" + act.getPantry() +
                "&number=" + act.getNumber();
    }

    private String readStream(InputStream in) {
        return in.toString();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}