package com.example.myapplication;

import java.io.Serializable;

public class Ingredient implements Serializable{

    private final int id;
    private final String name;
    private String amount;
    private String unit;

    private boolean active;

    public Ingredient(int id, String name)  {
        this.id= id;
        this.name = name;
        amount = "1";
        unit = "";
        this.active= true;
    }

    public Ingredient(int id, String name, boolean active)  {
        this.id= id;
        this.name = name;
        amount = "1";
        unit = "";
        this.active= active;
    }

    public Ingredient(int id, String name, String amount, String unit, boolean active)  {
        this.id= id;
        this.name = name;
        this.amount = amount;
        this.unit = unit;
        this.active= active;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return this.name +" ("+ this.amount +" "+ this.unit +")";
    }
}
