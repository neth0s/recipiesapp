package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageView = (ImageView) this.findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.chef);

        Button button1 = (Button) this.findViewById(R.id.button1);
        Button button2 = (Button) this.findViewById(R.id.button2);

        button1.setOnClickListener(v -> {
            Intent myIntent = new Intent(MainActivity.this, RecipiesActivity.class);
            //myIntent.putExtra("key", value); //Optional parameters
            MainActivity.this.startActivity(myIntent);
        });

        button2.setOnClickListener(v -> {
            finish();
            System.exit(0);
        });

        Log.i("CMS", "Page d'accueil");
    }
}